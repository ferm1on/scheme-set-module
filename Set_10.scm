; Name: Set_10, version 1.0 
(define version
  (list  
   "Version 1.0" "get-func" "put-func" "make-table" "get-data" "get-type" "change-set" "make-set" "belong?-set" 
   "add-set" "remove-set" "length-set" "index-set" "print-set"))
; Author: Michael Cunha

; Previous version        : There were no previous versions
; Changes in this version : N/A

; This Program implements a mathematical set handler where equality of elements depend on scheme's equal? function. 
; Currently this file has the following packages ready for installation
;    list-1.0-uncertified

; To add new underlying data structures to this program, simply install a new package implementing the basic functions.
; You should use the herein list package as a guide to creating your own packages. The pre and post conditions presented in the
; list package must be respected for all other packages to be created and carry the same version as this file
; Once you have included and tested your package, update line 12, column 5 of this file by appending the new packages' names 
; to the list of packages on the same line.
; 

; This program is organized in Layers. Each layer has some underlying assumptions for its containing definitions.
; Here is a description of each layer.
;    Layer 1: Layer where data structure level (basic) interfaces are maintained. Here is where all the different 
;             set packages are defined. If you are using a 3rd party package, only install packages whith the 
;             same version as this file.
;    Layer 2: In this layer, a look up table keeping track of the various packages is implemented as well as the most basic set
;             functions of which other set functions can be created. This layer is immutable. If this layer is changed, update
;             the program's version to indicate this change. Updating the program's version involves changing the file name by
;             changing the version name, changing the first line of this code to reflect your change, as well as updating the
;             version list with your new version number as well as any new underlying functions you have implemented
;             all set packages must be updated as well.
;    Layer 3: In this layer we install and load all nessesary parts for the set module to work. This is also the layer where
;             the user will be creating their own complex set operations such as union, intersection, sorting etc.

; Package installation functions names are in the form install-set-"package name"-"version"-"certificate condition"
; When developing your own packages. be aware that your make-set function must accept a list, and all other functions, if it
; expects a data type, you will be handed your own data type back. for example, if your packages data type is binary search tree,
; in every instance where you expect data, you will be handed a binary search tree. All other pre and post aspects will remain
; the same.
;------------------------------------------------------ LAYER 1 -----------------------------------------------------------
; Reference for install function is in SICP section 2.4 and 2.5

; Pre condition : table has been defined in the inheriting layer.
; Post condition: The list package has been installed in the table.
; Usage example : (install-set-list-1.0-uncertified)
;                 "List Package installed (uncertified)"
(define (install-set-list-1.0)
  
  ;              ********************************** List Data Structure Fuctions *******************************
  
  ; Pre condition : lst is a list.
  ; Post condition: returns a list where the elements in the list are the elements in lst if elements in lst are
  ;                 mutually exclusive, else returns #f
  ; Usage example : lst1 = (1 2 3 4), lst2 = (1 2 3 4 4)
  ;                 (make-set lst1) = (1 2 3 4)
  ;                 (make-set lst2) = #f
  (define (make-set lst)
    (let ((newlst (list )))
      (define (belong? data element)
        (cond
          ((null? data) #f)
          ((equal? (car data) element) #t)
          (else (belong?-set (cdr data) element))))
      (define (build-set to-add current)
        (cond
          ((null? to-add) current)
          ((belong? current (car to-add)) #f)
          (else (build-set (cdr to-add) (append current (list (car to-add)))))))
      (build-set lst (list ))))
  ; Certificats
  ; The certificate of this function depends on the correctness of build-set and belong?. however belong?'s correctness has been
  ; proven below which leaves us with the proof or build-set to develop. We will prove build-set by inducting on the invariant
  ; current = a proper Set in the form of a list and Elements of current + to-add = all elements of Set if the elements of lst
  ; make a proper set
  ; Basis step:
  ;    when build-set is first called all candidate elements of the possible Set are contained in to-add and current is an empty 
  ;    Set, therefore the variant remains true.
  ;    We consider the case where lst contains only 1 element. Obviusly this element won't be contained in the empty list current
  ;    and to-add is not null (not yet the end of the list). The recursive call will be envocked where current whill have in it
  ;    (car to-add) and to-add = (cdr to-add) = null, (build-set (cdr to-add) (append current (list (car to-add))))
  ;    At this stage, we are at the end of to-add and have processed all of to-add and we return current which is now the list 
  ;    representing the Set denoted by lst, observe how current is also a proper set.
  ;    In the case of a 2 element list lst = (element1 element2) and element1 = element2. We follow the same steps as above but
  ;    now the second iteration will execute the line ((belong? current (car to-add)) #f) which will stop the recursion and
  ;    return #f since lst does not denote a set.
  ; Induction step:
  ;    We now consider some list lst with arbitrary elements and length. If current has lenth n and we assume that the nth
  ;    recursive call worked correctly. Now we consider the nth + 1 call. current + to-add contains together all elements of lst
  ;    and current denotes a proper Set. for the n+1 element, if it is contained in current already, we terminate and return #f
  ;    since current will no longer a proper set and element must have accured 2 times in to-add. If (belong? current element) 
  ;    is false we move on to the next clause and add element to current and subtract it from to-add, and the invarient is still
  ;    preserved. thus, the function behaves as expected.
  
  ; Pre condition : data is a list. element is a scheme structure
  ; Post condition: If element is in data's top level returns #t, else returns #f.
  ; Usage example : element = a, data1 = {a b c + /), data2 = (v c + () (1 2))
  ;                 (belong?-set data1 element) = #t
  ;                 (belong?-set data2 element) = #f
  (define (belong?-set data element)
    (cond
      ((null? data) #f)
      ((equal? (car data) element) #t)
      (else (belong?-set (cdr data) element))))
  ; Certificate
  ; We prove the correctnes of belong?-set by inducting on data's length
  ; Basis step:
  ;    For an empty list, (null? data) will return true and belong?-set will return false since an empty set contains no elements.
  ;    For an one element list, if (equal? (car data) element) is true, we return #t since element belong in data. At this stage
  ;    there is nothing left to be done since data denotes a proper set and if element is cointained in it there will only be one
  ;    accurance of element.However if (equal? (car data) element) is falce, we call Belong?-set on the cdr of data which is 
  ;    itself null? marking the end of the list and we get #f as per the previus case.
  ; Induction step:
  ;    Asume belong?-set works correctly for the nth element. If we call the function on a list with n+1 elements.
  ;    if the nth+1 element in data = element than (car data) returns #t as expected else we calll belong?-set on (cdr data) which
  ;    where the answer must remain. Belcause belong?-set works correctly than we will get our expected answer.
  
  ; Pre condition : data is a list, element is a scheme structure
  ; Post condition: returns a new list where element is appended to data if element is not contained in data
  ;                 else returns data
  ; Usage example : element1 = (lambda (x) x), element2 = 2, data = (1 2 3 4) 
  ;                 (add-set data element1) = (1 2 3 4 (lambda (x) x))
  ;                 (add-set data element2) = (1 2 3 4)
  (define (add-set data element)
    (cond 
      ((null? data) (list element))
      ((equal? (car data) element) data)
      (else (cons (car data) (add-set (cdr data) element)))))
  ; Certificate
  ; we prove the correctnes of add-set by inducting on data's length
  ; Basis step:
  ;    We look at 2 cases. the first is when data is empty. if we call (add-set '() some-element), the clause (null? data)
  ;    will return #t and (list some-element) will be returned. which is indeed data + some-element.
  ;    The second case is when data has 1 element and itself has 2 subcases, if some-element is part of data or if some-element 
  ;    is not part of data.
  ;    Case 2a.
  ;        In the case that some-element is part of data, (equal? (car data) some-element)) will return true and data will than 
  ;        be returned unmodified, as axpected. We do not add repeat elements to a set. elements in a Set must be unique.
  ;    Case 2b.
  ;        In this case, some-element is not part of data and the else clause will be envoked. We contruct a new list where we
  ;        add (car data) to it and call add-set in the rest of the list. Because (cdr data) is empty add-set will return
  ;        (list some-element) and we end with the structure (cons (car data) (list some-element)) which is a list contaning 
  ;        all elements of data + the new element some-element.
  ;    As an observation, note that the code is searching for some-element in data, if it encounters it, it stops searching and 
  ;    returns the unmodified input data. This is the case because at any given recursion call. the invarient 
  ;    (cons (car data) (cdr data)) = data holds true. in each recursion we simply move across the list. if some-element is never
  ;    encountered we append it to the data.
  ; induction step:
  ;    Suppose now that the nth iteration of add-set works correctly, and we look at nth + 1 iteration. If some-element is to 
  ;    not be added to data, it would have not been already, because add-set works correctly for the nth iteration. This means then 
  ;    the addition of some-element to data depends solly on the nth + 1 or greated iteration of add-set. We consider 2 cases.
  ;    The case that some-element is found on the n + 1 iteration of add-set and the case it has not.
  ;    If some-element is found on data, than we correctly returns the nth + 1 data with its remaining elements. the nth recursion
  ;    will build (cons (car data) (cdr data)) which is data itself. If some-element is not found on the nth + 1 recursion,
  ;    than it must be on the rest of data and we call (else (cons (car data) (add-set (cdr data) element)), where (car data) is
  ;    the element we just locked at. If the n + 1 recursion recieves data = null, than some-element has not beend found and we
  ;    return (list element) which will be assembled on the nth recursion as (cons (car data) (list element)). Thus the function
  ;    behaves as expected.
  
  ; Pre condition : data is a list, element is a scheme structure
  ; Post condition: returns a new list where its items are the items of data minus element,
  ;                 if element does not belong in data returns data
  ; Usage example : data = (a b c d), element1 = c, element2 = 54 
  ;                 (remove-set data element1) = (a b d)
  ;                 (remove-set data element2) = (a b c d)
  (define (remove-set data element)
    (cond
      ((null? data) '())
      ((equal? (car data) element) (cdr data))
      (else (cons (car data) (remove-set (cdr data) element)))))
  ; Certificate
   ; we prove the correctnes of remove-set by inducting on data's length
  ; Basis step:
  ;    We look at 2 cases. the first is when data is empty. If we call (remove-set '() some-element), the predicate (null? data)
  ;    will return #t and an empty list will be returned. This is indeed what we expect, we remove no element from an empty set.
  ;    The second case is when data has 1 element and it has 2 subcases, if some-element is part of data or if some-element is 
  ;    not part of data.
  ;    Case 2a.
  ;        In the case that some-element is part of data, (equal? (car data) some-element)) will return #t and (cdr data) will then 
  ;        be returned. In the case of an 1 element list that is '(). This bevavior is what we expect since if we remove all
  ;        elements of a 1 element set we end up with the empty set.
  ;    Case 2b.
  ;        In this case, some-element is not part of data then else will be envoked. We contruct a new list where
  ;        (car data) is its first element and call remove-set in the rest of data. Because (cdr data) is empty 
  ;        remove-set will return '() and we will end up with the original list. Thus no element has been removed..
  ;    As an observation, note that the code is searching for some-element in data, if it encounters it, it stops searching and 
  ;    removes it by consing the elements of data to the right of some-element  with the elements of data to the left of 
  ;    some-element.
  ; induction step:
  ;    Suppose now that remove-set works correctly for some list of length n, for example (1 2 3 4... n), and we now call 
  ;    remove-set on the same list but with one extra entry (1 2 3 4... n n+1). If some-element is in the list, if it occured in 
  ;    the call < nth then it has been removed and the n+1th element != some-element, so n+1 will be appended to the end and the
  ;    recursion stops.
  ;    If some-element accurs in the n+1 possition, then ((equal? (car data) element) (cdr data)) will be invoked and 
  ;    (cdr data) = '() and the list returned will be all elements but the last as expected.
  
  ; Pre condition : data is a list.
  ; Post condition: returns the length of data.
  ; Usage example : data = (1 2 3 (4 5) 6)
  ;                 (length-set data) = 5
  (define (length-set data)
    (cond
      ((null? data) 0)
      (else (+ 1 (length-set (cdr data))))))
  ; Certificate
  ; We induct on the length of data
  ; Basis step:
  ;    if data = '(), the empty set, then (null? data) will return true and length-set will return 0. which is the size of the
  ;    empty set and what we expect.
  ; Induction step:
  ;    We asume that length-set works for the nth call on a set of size n + 1. By the nth call, length-set returns n as 
  ;    the set length. for the n + 1 call we have
  ;    (+ n 1 (length-set (cdr data))) = (+ n 1 (length-set '()) = (+ n 1 0) = n + 1 which is the correct length of a set with
  ;    n + 1 elements.
  
  ; Pre condition : data is a list, index is an integer where 1 <= index <= (length-set data).
  ; Post condition: returns the ith element of data.
  ; Usage example : data = (2 3 4 (lambda x x) b), i = 4
  ;                 (index-set data 4) = (lambda x x)
  (define (index-set data index)
    (cond
      ((equal? index 1) (car data))
      (else (index-set (cdr data) (- index 1)))))
  ; Certificate
  ; We induct on the length of data
  ; Basis step:
  ;    for a list data of length 1, index = 1 as well. (equal? index 1) returns #t and (car data) is returned which is what we expect.
  ; Induction ste:
  ;    we asume index-set works correctly for a list of length n. For a list n+1 where 1<= i <= length data. If index< n + 1 then
  ;    index-set has returned the correct element and the function terminated correctly. If index was originaly n + 1, then by the nth
  ;    call, index = 2 and data = (n n+1). "else" will be evoked and (index-set (cdr data) (- index 1)) will be called.
  ;    (index-set (n+1) (- 2 1)) = (index-set (n+1) 1). and by the recursion (equal? 1 1) will be evaluated to #t and 
  ;    (car (n+1)) = n+1 will be return which is the n+1th element with index = n + 1
  
  ; Pre condition : data is a list.
  ; Post condition: prints the elements in data separated by spaces and contained inside {}.
  ; Usage example : data = (1 2 3 4) 
  ;                 (print-set data) = { 1 2 3 4 }
  (define (print-set data)
    (define (print-member element)
      (display " ")
      (display element)
      (display " "))
    (define (print-body data)
      (cond
        ((null? data) (display "}"))
        (else (print-member (car data)) (print-body (cdr data)))))
    (display "{")
    (print-body data))
  ; Certiicate
  ; since print-member simply prints an element between 2 spaces " ". the correcness of print-set depends solely on the correcness
  ; of print-body. We prove the correctness of print-body by inducting in the size of data.
  ; Basis step:
  ;    in the case where data = '(). print-set will print { than call (print-body '()) which by the condition (null? '()) will
  ;    display }. in the end {} would have been printed which is indeed what we want. an empty set prints an empty set.
  ; Induction step:
  ;    supose the nth call of print-body works and we have printed {1 2 3 4... n so far. on the nth + 1 call. the else clause
  ;    will be evoked and we have (print-member (car data)) (print-body (cdr data)) = (print-member n+1) (print-body '())
  ;    (print-member n+1) = " n+1 " and (print-body '()) = "}" and we end with { 1  2  3  4...  n  n+1 } as expected.
  
  ;        ******************************** Installation (Inserting functions in table) ***********************************
  
  (put-func 'lst 'make-set make-set)
  (put-func 'lst 'print-set print-set)
  (put-func 'lst 'add-set add-set)
  (put-func 'lst 'remove-set remove-set)
  (put-func 'lst 'belong?-set belong?-set)
  (put-func 'lst 'length-set length-set)
  (put-func 'lst 'index-set index-set)
  
   "List Package installed")

(define (install-set-hash-1.0-uncertified)
  
;;Helper Functions, initial
 


;(define (create-empty-hash hash-max-size)
 ;      (define (iterate count)
  ;           (cond ((> count hash-max-size) '())
   ;          (else (cons (list #f 0) (iterate (+ 1 count))))))
    ;   (iterate 0))
;;-----------------------------------------------Built in Functions-----------------------------------------------------------------------

;;Make-set 
;The construction of a set in a hash table is a little bit more complex, but how I have done it is by having two values, since the 
;hash function gives the value of the index in which the data is contained. I have created a list where the first element of the list
;is saying whether this index has been used yet. i.e. (#f 0) would be indicative of an empty index, and (#t 0) would indicate that the value at
;that index is 0.
;Preconditions:  This function takes in a list of data, all the same type of data.
;Postconditions: Returns a set with a backbone made from a hash table, containing the contents of set that are different (i.e- no repeats)

  
  
(define (make-set lst)
  (define (create-empty-hash)
       (define (iterate count)
             (cond ((> count 127) '())
             (else (cons (list #f 0) (iterate (+ 1 count))))))
       (iterate 0))
   (define (iterate hash lst)
      (if (null? lst) hash 
                     (iterate (add-set hash (car lst)) (cdr lst))))
 (iterate (create-empty-hash) lst))
             
                  
;;belong?
;Preconditions: This function takes in an element and a list
;Postconditions: Returns a boolean value of #t when the element is already contained within the set, otherwise returns #f.
(define (belong?-set hash element)
  (cond ((null? hash) #f)
        ((car (car hash))
         (cond (( = element (cadr(car hash))) #t)
               (else (belong?-set (cdr hash) element))))
         (else (belong?-set (cdr hash) element))))
  
  
;;add-set
;Preconditions: This function takes an element and a hash
;Postconditions: Returns the hash containing the added element (if it did not already belong)
(define (add-set hash element)
  (define (iterate count hash)
        (let ((index (modulo element 127)))
        (if (= count index) 
            (cond ((caar hash) (cons (car hash) (cdr hash)))
                  (else (cons (list #t element) (cdr hash))))
           (cons (car hash) (iterate (+ count 1) (cdr hash))))))
  (iterate 0 hash))

  
  
  
;;remove-set
;Preconditions: This function takes an element and a hash
;Postconditions: Returns the hash with the aforementioned element removed.
(define (remove-set hash element)
      (define (iterate count hash)
        (let ((index (modulo element 127)))
        (if (= count index) 
            (cond (( = element (cadr(car hash))) (cons (list #f 0) (cdr hash)))
                  (else hash))
           (cons (car hash) (iterate (+ count 1) (cdr hash))))))
  (iterate 0 hash)
         )
        
  

  

;;length-set   
;Preconditions: This function takes a hash table.
;Postconditions: Returns the length of the hash table.   
  
(define (length-set hash)
  (define (counter count hash)
    (cond((null? hash) count) ;If hash is null, return count, which is initialized at 0, so an empty hash should return  0
         ((car (car hash)) (counter (+ 1 count) (cdr hash))) ;;If the car of the car of the hash is true, add one to count and continue on the cdr of the hash
         (else (counter count (cdr hash)))))
     
  (counter 0 hash))
  
  
  
  
;;index-set
;Preconditions: This function takes a hash table and a value, n, which represents the value of the index. 
;Postconditions: Returns the element at the nth index.
(define (index-set hash index)
    (cond
      ((eq? index 0) 
           (if (car (car hash)) (cadr (car hash)) (index-set (cdr hash) (- index 1))))
      (else (index-set (cdr hash) (- index 1)))))
 

;;print-set
;Preconditions: This function takes a hash
;Postconditions: Returns the printed contents of the hash table.

(define (print-set data)
    (define (print-member element)
      (display " ")
      (display (cadr element))
      (display " "))
    (define (print-body data)
      (cond
        ((null? data) (display "}"))
        ((NOT (car (car data))) (print-body (cdr data)))
        (else (print-member (car data)) (print-body (cdr data)))))
    (display "{")
    (print-body data))



;;Installing internal functions.
 (put-func 'hash 'make-set make-set)
 (put-func 'hash 'print-set print-set)
 (put-func 'hash 'add-set add-set)
 (put-func 'hash 'remove-set remove-set)
 (put-func 'hash 'belong?-set belong?-set)
 (put-func 'hash 'length-set length-set)
 (put-func 'hash 'index-set index-set)
 "Hash Table Package installed (uncertified)")

;------------------------------------------------------ LAYER 2 ----------------------------------------------------------
; Reference for this functions are in SICP section 3.3.3

(define (get-func key-1 key-2)
  (let ((subtable (assoc key-1 (cdr table))))
    (if subtable
        (let ((record (assoc key-2 (cdr subtable))))
          (if record (cdr record) false))
        false)))

(define (put-func key-1 key-2 value)
  (let ((subtable (assoc key-1 (cdr table))))
    (if subtable
        (let ((record (assoc key-2 (cdr subtable))))
          (if record
              (set-cdr! record value)
              (set-cdr! subtable (cons (cons key-2 value) (cdr subtable)))))
        (set-cdr! table (cons (list key-1 (cons key-2 value)) (cdr table)))))
  'ok)

(define (make-func-table)
  (list '*table*))

;     *************************************** Built in Functions *****************************************

; Pre condition : set = (make-set 'some-type some-data), and 'some-type package has been installed.
; Post condition: returns the data stored in set.
; Usage example : set = ('some-type . some-data)
;                 (get-data set) = some-data
(define (get-data set)
  (cdr set))

; Pre condition : set = (make-set 'some-type some-data), and 'some-type package has been installed.
; Post condition: returns the type of Set set is in the form 'type.
; Usage example : set = ( 'some-type . some-data)
;                 (get-type set) = 'some-type
(define (get-type set)
  (car set))

; Pre-Condition : set = (make-set 'some-type some-data), 'some-type package has been installed, 
;                 'new-type equal quoted ascii characters and new-type package has been installed.
; Post-Condition: Returns a set where its type is new-type
; Usage example : set = ('lst.data), new-type = 'tree
;                 (change-set 'tree set) = ('tree.data)
(define (change-set new-type set)
  (define (get-list set)
    (cond 
      ((= (length-set set) 0) (list ))
      (else (cons (index-set set 1) (get-list (remove-set set (index-set set 1)))))))
  (make-set new-type (get-list set)))

; Pre condition : type is a quoted ascii string and type package has been installed
;                 lst is a list.
; Post condition: create a set of Type type where the Set elements are the elements of list, 
;                 if lst is not a set returns #f instead.
; Usage example : list1 = (1 2 a (b c) +), list2 = (a a)
;               : (make-set 'tree list1) create a set of tree data type with elemetns 1 2 a (b c) +
;               : (make-set 'hash list2) returns #f since (a a) is not a set
(define (make-set type lst)
 (let ((newset (cons type ((get-func type 'make-set) lst))))
   (if (not (get-data newset)) #f newset)))

; Pre condition : set = (make-set 'some-type some-data), 'some-type package has been installed, 
;                 and element is a scheme structure.
; Post condition: returns #t if element is in the Set defined by set, else returns #f.
; Usage example : set = ('some-type . some-data), Set = {1 2 (1 2) 5), element1 = (1 2) element2 = 4
;                 (belong?-set element1 set) = #t
;                 (belong?-set element2 set) = #f
(define (belong?-set set element)
  ((get-func (get-type set) 'belong?-set) (get-data set) element))

; Pre condition : set = (make-set 'some-type some-data), 'some-type package has been installed.
;                 and element is a scheme structure.
; Post condition: if element is not in the Set defined by set returns a set constructed from Set U element.
;                 otherwise returns set.
; Usage example : set1 = (a b c d), set2 = (1 2 3 4), element = a
;                 (add-set set1 element) = (a b c d)
;                 (add-set set2 element) = (1 2 3 4 a)
(define (add-set set element)
  (cons (get-type set) ((get-func (get-type set) 'add-set) (get-data set) element)))

; Pre condition : set = (make-set 'some-type some-data), 'some-type package has been installed.
;                 and element is a scheme structure.
; Post condition: if element is in the Set defined by set returns a set constructed from Set - element.
;                 otherwise returns set.
; Usage example : set1 = (a b c d), set2 = (1 2 3 4), element = a
;                 (remove-set set1 element) = (b c d)
;                 (remove-set set2 element) = (1 2 3 4)
(define (remove-set set element)
    (cons (get-type set) ((get-func (get-type set) 'remove-set) (get-data set) element)))

; Pre condition : set = (make-set 'some-type some-data), 'some-type package has been installed.
; Post condition: returns an integer representing the number of elements in the Set defined by set
; Usage example : set = ( (+ -) (* /) (^ sqrt))
;                 (length-set set) = 3
(define (length-set set)
  ((get-func (get-type set) 'length-set) (get-data set)))

; Pre condition : set = (make-set 'some-type some-data), 'some-type package has been installed, 
;                 index <= (lenght-set set)
; Post condition: returns the ith element of the Set defined by set
; Usage example : set = ( (+ -) (* /) (^ sqrt)), index = 2
;                 (index-set set index) = (* /)
(define (index-set set index)
  ((get-func (get-type set) 'index-set) (get-data set) index))

; Pre condition : set = (make-set 'some-type some-data), 'some-type package has been installed,
; Post condition: prints the elements in Set defined by set separated by spaces and contained inside {}.
; Usage example : set = (1 2 3 4) 
;                 (print-set set) = { 1 2 3 4 }    
(define (print-set set)
  ((get-func (get-type set) 'print-set) (get-data set)))

;             ********************************* Initializing Function Table ***************************************

(define table (make-func-table))

;-------------------------------------------------- LAYER 3 ------------------------------------------------------------------

;                  *********************** Install Packages Here ****************************
; Available Packages
(install-set-list-1.0)
(install-set-hash-1.0-uncertified)

;                  *********************** User Code *************************************

; Pre Condition : type is a installed type, a and b are sets
; Post Condition: returns a new set of Type type where its elements are the intersection of a and b
; Usage Example : set1 = { 1 2 3 4 }, set2 = { 3 4 5 6 }, set3 = { 5 6 7 8 }
;                 (intersection 'lst set1 set2) = { 5 6 } of type 'lst
;                 (intersection 'lst set1 set3) = { } of type 'lst
(define (intersection type a b)
    (define (builder smallset bigset newset)
      (cond
        ((= (length-set smallset) 0) newset)
        ((belong?-set bigset (index-set smallset 1)) (builder (remove-set smallset (index-set smallset 1)) bigset (add-set newset (index-set smallset 1))))
        (else (builder (remove-set smallset (index-set smallset 1)) bigset newset))))
    (if (<= (length-set a) (length-set b)) 
        (builder a b (make-set type (list )))
        (builder b a (make-set type (list )))))